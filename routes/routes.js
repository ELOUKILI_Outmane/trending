var path = require("path");
const express = require("express");

var trending = require(path.resolve('./controllers/trendingController.js'));

module.exports = function (app) {
    var r = express.Router();

    r.route('/trending').get(trending.trending);

    app.use('/api', r);
}