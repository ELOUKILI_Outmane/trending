'use strict';

module.exports = {
    port: process.env.PORT || 3000,
    git_api_url: "https://api.github.com/search/repositories"
}