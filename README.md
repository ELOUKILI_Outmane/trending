# backend-coding-challenge

In this challenge, I used Node.js.

## installation

### cloning the github repository

```bash

git clone https://ELOUKILI_Outmane@bitbucket.org/ELOUKILI_Outmane/trending.git

```
### Install Node.js dependencies

Go inside project folder and open terminal and run the following command to install dependencies.

```bash

npm install

```
### run project

After the install dependencies is over, you'll be able to run the project, just run :

```bash

npm start

```

In postman put this URL for showing the result.

```bash

http://localhost:3000/api/trending

```
## Developed by 
    EL OUKILI Outmane.