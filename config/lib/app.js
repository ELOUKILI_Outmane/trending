var express = require("express");
var bodyParser = require("body-parser");
var path = require("path");
var config = require(path.resolve('./config/env/default.js'));
var app = express();

module.exports.start = function start(){

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
 
// parse application/json
app.use(bodyParser.json());

// require route
require(path.resolve('./routes/routes.js'))(app);

app.listen( config.port,() => console.log(`server running on ${config.port}....`) );
}