const fetch = require('node-fetch');
const path = require("path");
const moment = require("moment");
var config = require(path.resolve('./config/env/default.js'));

var laste_month_date = moment().subtract(1, 'month').format("YYYY-MM-DD");

exports.trending = function( req, res) {

    fetch(`${config.git_api_url}?q=created:>${laste_month_date}&sort=stars&order=desc`)
    .then(response =>
        response.json()
    )
    .then(trending =>{        
        res.status(200).jsonp({success: true, result: numberAndListOfRepo(trending)});
    })
    .catch(error =>{
        res.status(400).jsonp({success: false, message:error.message});
    })
}

/**
 * get list of language
 * @param {Object} trending 
 * @return {Array}
 */
function listOfLanguage(trending) {
    let language = [];
        trending.items.map(index =>{
            if(index.language !== null){
                if(language.indexOf(index.language) === -1){
                    language.push(index.language);
                }               

            }
        })
    return language;
}

/**
 * get object contain Number and list of repos using in language
 * @param {Object} trending
 * @return {Object}
 */
function numberAndListOfRepo(trending){
    let result = {};
    let language = listOfLanguage(trending);
    trending.items.map(index =>{
        if(language.indexOf(index.language) !== -1){
            let listOfRepo = [];
            let lang = index.language;
            if(result[lang]){
                if(result[lang].listOfRepo){
                    listOfRepo = result[lang].listOfRepo
                    listOfRepo.push(index.html_url)

                    result[lang] = {
                        numberOfRepo: result[lang].numberOfRepo + 1,
                        listOfRepo : listOfRepo
                    }
                }
            }
            else{
                listOfRepo.push(index.html_url)
                result[lang] = {
                    numberOfRepo: 1,
                    listOfRepo : listOfRepo
                }
            }            
        }
    })

    return result
}